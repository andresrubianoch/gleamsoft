package com.mituapps.gleamsoft.presentation.interfaces;

/**
 * Created by PORTATIL on 15/09/2016.
 */
public interface BasicSetUp {

    void initActionBar();
    void setNewTitle();
}
