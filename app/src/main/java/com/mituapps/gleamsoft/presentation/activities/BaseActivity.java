package com.mituapps.gleamsoft.presentation.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.mituapps.gleamsoft.R;
import com.mituapps.gleamsoft.presentation.navigation.Navigator;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.res.IntegerRes;

/**
 * Created by Andres Rubiano Del Chiaro on 14/09/2016.
 */


@EActivity
public class BaseActivity extends AppCompatActivity{
    Navigator mNavigator;

    public BaseActivity() {
        this.mNavigator = new Navigator();
    }

    protected void changeTitle(@NonNull CollapsingToolbarLayout toolbar, int idText){
        toolbar.setTitle(getString(idText));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, SignInActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void setUpActionBar(@NonNull Toolbar toolbar){
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
