package com.mituapps.gleamsoft.presentation.activities;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;

import com.mituapps.gleamsoft.R;
import com.mituapps.gleamsoft.presentation.interfaces.BasicSetUp;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_forget_password)
public class ForgetPasswordActivity extends BaseActivity implements BasicSetUp{

    @ViewById
    Toolbar toolbarForgetPassword;

    @ViewById
    CollapsingToolbarLayout collapserForgetPassword;

    @Click
    public void buttonForgetPasswordAccept(){
        ForgetPasswordActivity.this.finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initActionBar();
        setNewTitle();
    }

    @Override
    public void initActionBar() {
        setUpActionBar(toolbarForgetPassword);
    }

    @Override
    public void setNewTitle() {
        changeTitle(collapserForgetPassword, R.string.signup_register_two);
    }
}
