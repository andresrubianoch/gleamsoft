package com.mituapps.gleamsoft.presentation.activities;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;

import com.mituapps.gleamsoft.R;
import com.mituapps.gleamsoft.presentation.interfaces.BasicSetUp;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_sign_up)
public class SignUpActivity extends BaseActivity implements BasicSetUp{
    @ViewById
    Toolbar toolbarSignUp;

    @ViewById
    CollapsingToolbarLayout collapserSignUp;

    @Click
    public void buttonSignupAccept(){
        SignUpActivity.this.finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initActionBar();
        setNewTitle();
    }

    @Override
    public void initActionBar() {
        setUpActionBar(toolbarSignUp);
    }

    @Override
    public void setNewTitle() {
        changeTitle(collapserSignUp, R.string.signup_register_two);
    }
}
