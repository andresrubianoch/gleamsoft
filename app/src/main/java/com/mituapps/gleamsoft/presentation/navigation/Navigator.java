package com.mituapps.gleamsoft.presentation.navigation;

import android.content.Context;

import com.mituapps.gleamsoft.presentation.activities.ForgetPasswordActivity_;
import com.mituapps.gleamsoft.presentation.activities.HomeActivity_;
import com.mituapps.gleamsoft.presentation.activities.SignInActivity_;
import com.mituapps.gleamsoft.presentation.activities.SignUpActivity_;

/**
 * Created by Andres Rubiano Del Chiaro on 14/09/2016.
 */
public class Navigator {

    public Navigator() {
    }

    public void toForgetPasswordActivity(Context context) {
        if (null != context)
            ForgetPasswordActivity_.intent(context).start();
    }

    public void toHomeActivity(Context context) {
        if (null != context)
            HomeActivity_.intent(context).start();
    }

    public void toSignInActivity(Context context) {
        if (null != context)
            SignInActivity_.intent(context).start();
    }

    public void toSignUpActivity(Context context) {
        if (null != context)
            SignUpActivity_.intent(context).start();
    }


}
