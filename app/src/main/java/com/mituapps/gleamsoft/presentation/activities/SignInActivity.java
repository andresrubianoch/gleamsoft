package com.mituapps.gleamsoft.presentation.activities;

import android.view.MenuItem;

import com.mituapps.gleamsoft.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_sign_in)
public class SignInActivity extends BaseActivity {

    @Click
    public void mTxtSignInTaxi(){
        mNavigator.toSignUpActivity(SignInActivity.this);
    }

    @Click
    public void mTxtSignInCar(){
        mNavigator.toSignUpActivity(SignInActivity.this);
    }

    @Click
    public void buttonSignInForgetPassword(){
        mNavigator.toForgetPasswordActivity(SignInActivity.this);
    }

    @Click
    public void buttonSignInAccept(){
        mNavigator.toHomeActivity(SignInActivity.this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       return false;
    }

}
