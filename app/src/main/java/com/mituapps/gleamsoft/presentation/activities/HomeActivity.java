package com.mituapps.gleamsoft.presentation.activities;

import com.mituapps.gleamsoft.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_home)
public class HomeActivity extends BaseActivity {

    @Click
    public void buttonHomeCloseSession(){
        HomeActivity.this.finish();
    }
}
